package glab

import (
	"tlab/components"

	"github.com/charmbracelet/bubbles/list"
)

var Options = []list.Item{
	components.NewItem("Issues", "Create, Manage & View Your Issues"),
	components.NewItem("Merge Rquest", "Create, Approve & Deny Merge Requests"),
	components.NewItem("Branches", "Create, Remove, and View Branches"),
	components.NewItem("Members", "Manage Members"),
}
